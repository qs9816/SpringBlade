package org.springblade.common.base;

import org.springblade.core.base.controller.BladeController;
import org.springblade.core.constant.ConstCache;
import org.springblade.core.constant.ConstCacheKey;
import org.springblade.core.constant.ConstCurd;

/**
 * 用于拓展controller类
 */
public class BaseController extends BladeController implements ConstCurd, ConstCache, ConstCacheKey {

}
